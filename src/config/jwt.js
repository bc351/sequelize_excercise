import jwt from "jsonwebtoken";
// tao token
const generateToken = (data) => {
    // 1: du lieu muon ma hoa thanh token => payload
    // 2: secret key => signature
    // 3: han su dung, thuat toan => header
//   return jwt.sign("hehe hihi","HELLO WORLD","");
// HS256: truyen chuoi(mac dinh)
// ES256: truyen 1 du lieu kha dai cai nay thi tranh viec lam gia token
  return jwt.sign({data:data},"FLASH",{expiresIn: "5m",algorithm:"HS256"});
};
// kiem tra tinh hop le cua token
const checkToken = (token) => {
// secret key va nhung thong tin nhay cam, it thay doi thuong duoc luu o .env
// tham so 1: token,2: secret key,
  return jwt.verify(token,"FLASH",);
};
// giai ma token
const decodeToken = (token) => {
  return jwt.decode(token);
};
const tokenApi = (req,res,next) => { 
    try {
     let {token} = req.headers
     // kiem tra token
     console.log(checkToken(token));
     if(checkToken(token)){
    // dung => next()
       next()
     }
    } catch (error) {
    // sai thi bao
     res.status(401).send(error.message)
    }
    }
export { generateToken, checkToken, decodeToken, tokenApi };
