// noi luu cau truc ket noi co so du lieu
import {Sequelize} from 'sequelize'
import config from"../config/config.js"

// lop doi tuong la class
// const connect = mysql.createConnection({
//   host: "localhost",
//   user: "root",
//   password: "1234",
//   port: "3306",
//   database: "db_node32",
// });
let sequelize = new Sequelize(config.database,config.user,config.pass,{
    host: config.host,
    dialect:config.dialect,
    port:config.port
})

// check xem ket noi co dung hay k
try {
   await sequelize.authenticate();
   console.log("Successful connected"); 
} catch (error) {
    console.log("Fail to connect");
}
// node src/models/index.js
export default sequelize

// yarn sequelize-auto -h localhost -d db_food -u root -x 1234 -p 3306 --dialect mysql -o src/models -l esm
