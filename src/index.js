// chua server
//  dinh nghia server se nam o day
// package.json luu ten thu vien
// yarn init tao ra package json
// ghi "type":"module" trong package json
// yarn add express
import express from "express";
import mysql from "mysql2";
import cors from "cors"
import { getNguoiDung } from "./controllers/userController.js";
import rootRouter from "./routers/rootRouter.js";
const app = express();
// yarn add swagger-ui-express swagger-jsdoc
import swaggerUi from 'swagger-ui-express';
import swaggerJsDoc from 'swagger-jsdoc';

// port quoc dan
// node index.js
// yarn add nodemon =>auto reset server khi save
// tao API => Endpoint, method
app.listen(8080);
// middle ware => nhung dong lenh, ham chinh giua de xu ly 1 van de gi do
app.use(express.json());
// middleware chap nhan cho tat ca domain FE truy cap vao BE nay la * nhung neu mac dinh de origin thi chi co domain do moi truy cap dc vao BE thoi
app.use(cors({
    origin:['http://localhost:5500','http://127.0.0.1:5500']
}))
// dinh vi thu muc de load tai nguyen tu BE
app.use(express.static("."))
// app.method bat ky
// GET: /demo

app.get("/demo/:id/:hoTen", (req, res) => {
  // request
  // lay tu params: lay data tren url,
  // query string: /demo?id=1&hoTen=abc phu thuoc vao FE,query la cua BE con lai la cua FE(chi truyen 1-2 bien)
  let { id1, hoTen1 } = req.query;
  // query params:  /demo/1/abc phu thuoc vao BE
  // (chi truyen 1-2 bien)
  let { id, hoTen } = req.params;
  // neu truyen nhieu qua thi phai sd body
  // body: lay data qua cau truc JSON =>thuong POST, PUT, DELETE
  let { userId, userName, email, phone } = req.body;
  // data tra tu BE ve FE
  res.send(hoTen + " - " + id);
  // string, object, list object,.... tru number(vi trung vs status code)
});

app.post("/demo2/:id", (req, res) => {
  let { userId, userName, email, phone } = req.body;
  res.status(200).send({
    userId,
    userName,
    email,
    phone,
  });
});

//  ket noi CSDL va truy van du lieu
// yarn add mysql2(mysql2 la cai ten thoi chu k ph la he co so du lieu t2)
// const connect = mysql.createConnection({
//   host: "localhost",
//   user: "root",
//   password: "1234",
//   port: "3306",
//   database: "db_node32",
// });
// quy tac dat ten endpoint: viet thuong cach nhau boi gach ngang
// bat dong bo asynchronous
// localhost:8080/api /user /get-nguoi-dung
// user =>root router
// get-nguoi-dung => userRouter
// (req,res) => {  }=>userController

// yarn add cors


// http://localhost:8080/api/user/get-nguoi-dung


// app.get('/api/user/get-nguoi-dung',(req,res)=>{
//   res.send("Get user")
// })

// doi thanh use vi sd middleware de truy cap
app.use("/api",rootRouter)

// yarn add multer

// swagger


const options = {
    definition: {
        info: {
            title: "api",
            version: "1.0.0"
        }
    },
    apis: ["src/swagger/index.js"]
}

const specs = swaggerJsDoc(options);

app.use("/swagger", swaggerUi.serve, swaggerUi.setup(specs));



/**
 * @swagger
 * /api/v1/user/getUser:
 *  get:
 *      description: responses
 *      tags: [User]
 *      responses:
 *          200: 
 *              description: success   
 */

/**
 * @swagger
 * /api/v1/user/updateUser/{id}:
 *  put:
 *      description: responses
 *      tags: [User]
 *      parameters:
 *      - in: path
 *        name: id
 *      - in: body
 *        name: user
 *        schema:
 *           type: object
 *           properties:
 *             userName:
 *               type: string
 *             firstName:
 *               type: string
 *             lastName:
 *               type: string
 *      responses:
 *          200: 
 *              description: res   
 */