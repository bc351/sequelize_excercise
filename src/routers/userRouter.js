import {
  createNguoiDung,
  getNguoiDung,
  updateNguoiDung,
  removeNguoiDung,
  getUserPage,
  signUp,
  login,
} from "../controllers/userController.js";
import express from "express";
import multer from "multer";
// File system FS
import fs from "fs";
import upload from "../controllers/uploadController.js";
import { tokenApi } from "../config/jwt.js";
// localhost:8080/api/user/upload
const userRouter = express.Router();
userRouter.post("/upload", upload.single("file"), (req, res) => {
  let file = req.file;
  // chi luu ten fileName vao CSDL
  // luu database
  // model.user.update({avatar:file.fileName})
  res.send(file);
});

// API upload hinh food
userRouter.post("/base64", upload.single("file"), (req, res) => {
  // fs.writeFile(process.cwd()+ "/test.txt","Hello World", () => {  })
  // fs.appendFile()
  // de tranh callBack Hell cac ham long cheo nhau thi nen tach ra bang cach sd async await :)))
  let file = req.file;
  fs.readFile(process.cwd() + "/public/img/" + file.filename, (err, data) => {
    // parse base64
    let newName = `data:"${file.mimetype};base64, ${Buffer.from(data).toString(
      "base64"
    )}`;
    res.send(newName);
  });
  // fs.readFile()
  // fs.copyFile()
  // fs.unlink()
});

// userRouter.get("/user/get-nguoi-dung", getNguoiDung")
// neu khong goi next thi se chay hoai
userRouter.get("/get-nguoi-dung", getNguoiDung);

userRouter.get("/get-user-page/:page/:pageSize",tokenApi,getUserPage);
userRouter.post("/create-nguoi-dung",tokenApi,createNguoiDung);
userRouter.put("/update-nguoi-dung/:food_id",tokenApi,updateNguoiDung);
userRouter.delete("/delete-nguoi-dung/:food_id", removeNguoiDung);

// Pagination
userRouter.get("/get-user-page/:page/:pageSize", getUserPage);
// app.route

userRouter.post("/sign-up", signUp);
userRouter.post("/login", login);
export default userRouter;

// them du lieu vao table user => Create
// userRouter.post("/signup")

// kiem tra du lieu trong table user => Read
// userRouter.post("/login")
