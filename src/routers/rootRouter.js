import express from "express";
import userRouter from './userRouter.js';
// Noi quan ly ten doi tuong cua endpoint
const rootRouter= express.Router()

rootRouter.use("/user",userRouter)
// rootRouter.use("/product",productRouter)

export default rootRouter
