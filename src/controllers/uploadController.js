
import multer from "multer";

// File system FS
import fs from "fs";
// __dirname: tra ve duong dan file dang dung
// process.cwd(): tra ve duong dan goc: node32_sequelize
const storage = multer.diskStorage({
  destination: process.cwd() + "/public/img",
  filename: (req, file, callback) => {
    // neu ten file hinh qua xau devs phai custom lai ten
    let newName = new Date().getTime() + "_" + file.originalname;
    callback(null, newName);
    // doi ten file truoc khi vao request
  },
});
const upload = multer({
  // destination: khai bao duong dan luu tai nguyen o BE
  //  storage: storage rut ngan lai
  storage,
});

export default upload