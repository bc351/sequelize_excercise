// import Food from "../models/food.js";
import initModels from "../models/init-models.js";
import sequelize from "../models/index.js";
import bcrypt from "bcrypt"
// Op
// enviroment <=> bien moi truong
const models = initModels(sequelize);
import { Sequelize } from "sequelize";
import { errorCode, failCode, successCode } from "../config/response.js";
import { generateToken } from "../config/jwt.js";

const Op = Sequelize.Op;
const getUserPage = async (req, res) => {
  try {
    let { page, pageSize } = req.params;
    let index = (page - 1) * pageSize;
    // SELECT * FROM user LIMIT index, pageSize
    // tong so luong(9) /tong sl tren trang(6) =1.5 =2
    let data = await models.user.findAll({
      offset: index,
      limit: Number(pageSize),
    });
    successCode(res, data, "Sucess");
  } catch (error) {
    errorCode(error, "BE fail");
  }
};
const getNguoiDung = async (req, res) => {
  // co the su dung cac ham truy van cua sequelize
  // tuong ung vs cau lenh SELECT * from food
  // dong bo hoa bang cach sd async await la cho roi hay res.send(data) thi se show ra dc data
  // findAll tra ve list object
  // cau lenh SELECT * from food where food_id< 5
  // cau lenh SELECT * from food where food_name LIKE "%a%"

  // SELECT * FROM food JOIN food_type ON ... WHERE food_type
  try {
    // sequelize.query("SELECT * FROM food WHERE food_id=`${abc}` or food_id=\'abc'\")
    // support mysql normal language
    let listFood = await models.user.findAll({
      include: ["food_id_foods"],
    });

    let data = await models.user.findAll({
      where: {
        user_id: {
          [Op.lt]: 5,
        },
      },
    });
    // findOne tra ve 1
    let data1 = await models.user.find;
    // localhost:8080/api/user/get-nguoi-dung
    successCode(res, listFood, "Sucessfully get data");
  } catch (error) {
    errorCode(error, "Fail to load data the fault from BE");
  }
};

// create
const createNguoiDung = async (req, res) => {
  // Insert into food Values{}
  // client data request body

  try {
    let { full_name, email, password } = req.body;
    let newData = {
      full_name,
      email,
      password,
    };
    let data = await models.user.create(newData);
    // food_id co hoac khong cung duoc

    successCode(res, data, "Sucessfully created user");
  } catch (error) {
    errorCode(error, "BE Fault");
  }
};

//  Update
const updateNguoiDung = async (req, res) => {
  // luu y food_id thi se lay tu params

  try {
    let { full_name, email, password } = req.body;
    let { user_id } = req.params;
    await models.user.update(
      {
        full_name,
        email,
        password,
      },
      { where: { user_id } }
    );
    res.status(200).send("Sucessfully updated");
  } catch (error) {
    res.status(500).send("Loi BE");
    console.log(error);
  }
};

// Delete
const removeNguoiDung = async (req, res) => {
  try {
    // check ton tai
    let { user_id } = req.params;
    let checkUser = await models.user.findAll({ where: { user_id } });
    if (checkUser.length > 0) {
      await models.user.destroy({ where: { user_id } });
      res.send("Successfully deleted");
    }
  } catch (error) {
    res.status(500).send("Loi BE");
  }
};
const signUp = async (req, res) => {
  try {
    let { full_name, email, pass_word } = req.body;
    // kiem tra email co ton tain hay khong
    // findAll tra ve []
    // findOne tra ve {}
    let checkEmail = await models.user.findAll({
      where: {
        email,
      },
    });
    if (checkEmail.length > 0) {
      // 400
      failCode(res, "","Email already exist")
    } else {
      let newUser = { 
        full_name, 
        email,
        pass_word: bcrypt.hashSync(pass_word,10) }
        // pass_word deu duoc ma hoa
      await models.user.create(newUser);
      successCode(res, "", "Successfully sign up");
    }
  } catch (error) {
    errorCode(res, "BE fail");
  }
};
const login = async(req, res) => {
  let dNow = new Date()
  dNow.setMinutes(dNow.getMinutes()+5)
  // kiem tra email vs password
  let {email,pass_word}= req.body
  const checkUser = await models.user.findOne({
    where: {
      email,
    }
  })
  if(checkUser){
    // login thanh cong
    // check pass
    // tham so dau la pass_word user nhap chua ma hoa
    // tham so thu 2 la pass_word da ma hoa trong database
    if(bcrypt.compareSync(pass_word,checkUser.pass_word)){
      let token = generateToken(checkUser)
      successCode(res,token,"Successfully login")
      // checkUser = {...checkUser,pass_word:""}
    }else{
      failCode(res,"","Incorrect Password!")
    }
  }else{
    // email hoac password sai
    failCode(res,"","Incorrect Email!")
  }
};
export {
  login,
  signUp,
  getNguoiDung,
  createNguoiDung,
  updateNguoiDung,
  removeNguoiDung,
  getUserPage,
};

// chuc nang quen mat khau thi tao ra 1 table trong database de quan ly quen mat khau
// da so cac cach he thong deu xai la gui ma ve email hoac sdt
// cac buoc lam:
// tao bang quen mk gom 3 truong: id, ma: generate ra ngau nhien, han su dung
// khi nguoi dung nhap email hoac so dt vao csdl ktra xem co ton tai k, sau do create ra 1 dong du lieu ten la code: ADSBASD se co thoi han trong bao lau: vd 5p
// gui mail cai code nay ve sdt hoac email ve ng dung hoac goi dien thoai ve gui code cho nguoi dung
// sau do ho nhap ma va nhan tiep tuc
// quen mk => nhap email => nhap code xac nhan
// kiem tra code trong table, va kiem tra han sd neu co
// co het tat ca thi re-direct ho ve tao mk moi
// va thuc hien chuc nang update mk moi
// 2 API 1 API gui code ve
// 1 API tao moi 
